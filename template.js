/*global console, exports*/

(function () {
  "use strict";

  // Template description
  exports.description = "Create a WordPress plugin";

  // Template notes (displayed before prompts)
  exports.notes = "Generates a plugin based on the Widgit default boilerplate.";

  // Post-prompt notes
  exports.after = "Plugin generation finished.";

  // Any existing file matching this will warn
  exports.warnOn = "*";

  // Initialize all the things!
  exports.template = function(grunt, init, done) {
    init.process({}, [
      // Prompts
      init.prompt("pluginname", "Plugin Name"),
      init.prompt("pluginslug", "Plugin Slug"),
      init.prompt("plugindesc", "Plugin Description")
    ], function(err, props) {
      props.pluginsafename = props.pluginname.replace(/[\W_]+/g, "_");
      props.pluginpackage = props.pluginsafename.replace(/_/g, "");
      props.pluginfunction = props.pluginslug.replace(/-/g, "_");
      props.pluginconstant = props.pluginfunction.toUpperCase();

      // Files to copy (and process)
      var files = init.filesToCopy(props);
      console.log(files);

      // Actually copy (and process) files
      init.copyAndProcess( files, props );

      done();
    });
  };
}());