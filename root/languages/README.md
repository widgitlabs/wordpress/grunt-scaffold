# {%= pluginname %}

This folder contains translation files for {%= pluginname %}.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/{%= pluginslug %}`.
