# {%= pluginname %}

## Welcome to our GitLab Repository



### Installation

1. You can clone the GitLab repository: `https://gitlab.com/widgitlabs/wordpress/{%= pluginslug %}.git`
2. Or download it directly as a ZIP file: `https://gitlab.com/widgitlabs/wordpress/{%= pluginslug %}/-/archive/master/{%= pluginslug %}-master.zip`

This will download the latest developer copy of {%= pluginname %}.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/{%= pluginslug %}/issues?state=open)!

### Contributions

Anyone is welcome to contribute to {%= pluginname %} Please read the
[guidelines for contributing](https://gitlab.com/widgitlabs/wordpress/{%= pluginslug %}/-/blob/master/CONTRIBUTING.md)
to this repository.
