<?php
/**
 * Core unit test
 *
 * @package     {%= pluginpackage %}\Tests\Core
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       1.0.0
 */
class Tests_{%= pluginsafename %} extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = {%= pluginsafename %}();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the {%= pluginsafename %} instance
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_{%= pluginfunction %}_instance() {
		$this->assertClassHasStaticAttribute( 'instance', '{%= pluginsafename %}' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( {%= pluginconstant %}_VER, '1.0.0' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( {%= pluginconstant %}_URL, $path );

		// Plugin folder path.
		$path               = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path               = substr( $path, 0, -1 );
		${%= pluginfunction %} = substr( {%= pluginconstant %}_DIR, 0, -1 );
		$this->assertSame( ${%= pluginfunction %}, $path );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'class-{%= pluginslug %}.php' );

		/** Check Assets Exist */
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'assets/banner-772x250.jpg' );
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'assets/banner-1544x500.jpg' );
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'assets/icon.svg' );
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'assets/icon-128x128.jpg' );
		$this->assertFileExists( {%= pluginconstant %}_DIR . 'assets/icon-256x256.jpg' );
	}
}
