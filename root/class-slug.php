<?php
/**
 * Plugin Name:     {%= pluginname %}
 * Plugin URI:      http://gitlab.com/widgitlabs/wordpress/{%= pluginslug %}/
 * Description:     {%= plugindesc %}
 * Author:          Widgit Labs
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     {%= pluginslug %}
 * Domain Path:     languages
 *
 * @package         {%= pluginpackage %}
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( '{%= pluginsafename %}' ) ) {


	/**
	 * Main {%= pluginsafename %} class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	class {%= pluginsafename %} {


		/**
		 * The one true {%= pluginsafename %}
		 *
		 * @var         {%= pluginsafename %} $instance The one true {%= pluginsafename %}
		 * @since       1.0.0
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @var         object $settings The settings object
		 * @since       1.0.0
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      object self::$instance The one true {%= pluginsafename %}
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof {%= pluginsafename %} ) ) {
				self::$instance = new {%= pluginsafename %}();
				self::$instance->setup_constants();
				self::$instance->hooks();
				self::$instance->includes();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', '{%= pluginslug %}' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', '{%= pluginslug %}' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( '{%= pluginconstant %}_VER' ) ) {
				define( '{%= pluginconstant %}_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( '{%= pluginconstant %}_DIR' ) ) {
				define( '{%= pluginconstant %}_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( '{%= pluginconstant %}_URL' ) ) {
				define( '{%= pluginconstant %}_URL', plugin_dir_url( __FILE__ ) );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global ${%= pluginfunction %}_options;

			// Load settings handler if necessary.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				require_once {%= pluginconstant %}_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php';
			}

			require_once {%= pluginconstant %}_DIR . 'includes/admin/settings/register-settings.php';

			self::$instance->settings   = new Simple_Settings( '{%= pluginfunction %}', 'settings' );
			${%= pluginfunction %}_options = self::$instance->settings->get_settings();
		}


		/**
		 * Internationalization
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( '{%= pluginfunction %}_lang_dir', $lang_dir );

			// Traditional WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), '' );
			$mofile = sprintf( '%1$s-%2$s.mo', '{%= pluginslug %}', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/{%= pluginslug %}/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/{%= pluginslug %}/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/{%= pluginslug %}/ folder.
				load_textdomain( '{%= pluginslug %}', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/{%= pluginslug %}/languages/ folder.
				load_textdomain( '{%= pluginslug %}', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/{%= pluginslug %}/ folder.
				load_textdomain( '{%= pluginslug %}', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( '{%= pluginslug %}', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true {%= pluginsafename %}
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php ${%= pluginfunction %} = {%= pluginsafename %}(); ?>
 *
 * @since       2.0.0
 * @return      {%= pluginsafename %} The one true {%= pluginsafename %}
 */
function {%= pluginfunction %}() {
	return {%= pluginsafename %}::instance();
}

// Get things started.
{%= pluginsafename %}();
