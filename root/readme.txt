=== {%= pluginname %} ===
Contributors: widgitlabs, evertiro
Donate link: https://evertiro.com/donate/
Tags:
Requires at least: 3.0
Tested up to: 5.4.1
Stable tag: 1.0.0

{%= plugindesc %}

== Description ==



== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Search for '{%= pluginname %}'
3. Activate {%= pluginname %} from your Plugins page

= From WordPress.org =

1. Download {%= pluginname %}
2. Upload the '{%= pluginslug %}' folder to the '/wp-content/plugins' directory of
   your WordPress installation
3. Activate {%= pluginname %} from your Plugins page

== Frequently Asked Questions ==

None yet

== Changelog ==

= Version 1.0 =
* Initial release
